//
//  AppDelegate.h
//  CoreAnimation
//
//  Created by Aron Crittendon on 6/23/15.
//  Copyright (c) 2015 acrittendon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

